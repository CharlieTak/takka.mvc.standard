﻿tinymce.init({
    selector: "textarea.tinymce",
    plugins: [
"advlist autolink lists link image charmap print preview anchor",
"searchreplace visualblocks code fullscreen",
"insertdatetime media table contextmenu paste",
"code"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code",
    convert_urls: false
});

$(".datecontrol").datepicker({ language: "en-GB" });