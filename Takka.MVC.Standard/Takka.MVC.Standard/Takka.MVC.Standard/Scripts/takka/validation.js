﻿if (jQuery.validator) {

    // The boolean required function
    jQuery.validator.unobtrusive.adapters.add("brequired", function (options) {
        //b-required for checkboxes
        if (options.element.tagName.toUpperCase() == "INPUT" && (options.element.type.toUpperCase() == "CHECKBOX" || options.element.type.toUpperCase() == "RADIO")) {
            //setValidationValues(options, "required", true);
            options.rules["required"] = true;
            if (options.message) {
                options.messages["required"] = options.message;
            }
        }
    });

    // The unlike function
    jQuery.validator.addMethod(
        'unlike',
       function (value, element, params) {
           // No check on optional as this always evaluates to true if an element is blank such as a checkbox
           //if (!this.optional(element)) {
               var returnValue = true;
               var props = params.otherproperty.split(',');
               var ignorevalue = params.ignorevalue;
               var propertyName = params.propertyname;
                // Allow for checkboxes returning undefined for unticked
               if (params.compareType == "CHECKBOX" && value == undefined) {
                   value = "false";
               }

               if (value == ignorevalue)
               {
                   return true;
               }
               // Cycle through our unlike properties and see if they match. If any match return error.
               for (var i = 0; i < props.length; i++) {
                   var otherPropertyType = $("input[type='radio'][name='" + props[i] + "']").length > 0 ? "RADIO" : $("input[type='checkbox'][name='" + props[i] + "']").length > 0 ? "CHECKBOX" : "INPUTORSELECT";
                   var otherProperty = $('#' + props[i]);
                   // Allow for checkboxes in other property
                   var otherPropertyValue = otherPropertyType == "CHECKBOX" ?  otherProperty.is(":checked").toString() :  (otherPropertyType == "RADIO" &&  !otherProperty.is(":checked")) ? "---do not validate---" : otherProperty.val();
                   if (otherPropertyValue == value) {
                       returnValue = false;
                       return returnValue;
                   }
               }
           //}
           return true;
       });

    jQuery.validator.unobtrusive.adapters.add(
        'unlike', ['otherproperty', 'otherpropertyname', 'ignorevalue', 'propertyname'], function (options) {
            var params = {
                otherproperty: options.params.otherproperty,
                otherpropertyname: options.params.otherpropertyname,
                ignorevalue: options.params.ignorevalue,
                propertyname: options.params.propertyname,
                compareType: $("input[type='radio'][name='" + options.params.propertyname + "']").length > 0 ? "RADIO" : $("input[type='checkbox'][name='" + options.params.propertyname + "']").length > 0 ? "CHECKBOX" : "INPUTORSELECT"
            };
            options.rules['unlike'] = params;
            options.messages['unlike'] = options.message;
        });



    
    // The if property set required function
    jQuery.validator.addMethod(
        'ifpropertysetrequired',
       function (value, element, params) {
               var returnValue = true;
               var propertytocompare = params.propertytocompare;
               var propertyvalueforrequired = params.propertyvalueforrequired;
               var propertytocompareValue = params.compareType == "RADIO" ? $("input[type='radio'][name='" + propertytocompare + "']:checked").val() : params.compareType == "CHECKBOX" ? $("input[type='checkbox'][name='" + propertytocompare + "']:checked").val() : $('#' + propertytocompare).val();
                // Allow for unticked checkboxes
               if (params.compareType == "CHECKBOX" && propertytocompareValue == undefined)
               {
                   propertytocompareValue = "false";
               }
               if (propertytocompareValue == propertyvalueforrequired)
               {
                   if(value == null || value == undefined || value.length == 0)
                   {
                       return false;
                   }
               }
           return true;
       });

    jQuery.validator.unobtrusive.adapters.add(
        'ifpropertysetrequired', ['propertytocompare', 'propertyvalueforrequired'], function (options) {
            var params = {
                propertytocompare: options.params.propertytocompare,
                propertyvalueforrequired: options.params.propertyvalueforrequired,
                compareType: $("input[type='radio'][name='" + options.params.propertytocompare + "']").length > 0 ? "RADIO" : $("input[type='checkbox'][name='" + options.params.propertytocompare + "']").length > 0 ? "CHECKBOX" : "INPUTORSELECT"
            };
            options.rules['ifpropertysetrequired'] = params;
            options.messages['ifpropertysetrequired'] = options.message;
        });

    // The if property set required function
    jQuery.validator.addMethod(
        'ifpropertysetnotallowed',
       function (value, element, params) {
           var returnValue = true;
           var propertytocompare = params.propertytocompare;
           var ignorevalues = params.ignorevalues;
           var propertytocompareValue = params.compareType == "RADIO" ? $("input[type='radio'][name='" + propertytocompare + "']:checked").val() : params.compareType == "CHECKBOX" ? $("input[type='checkbox'][name='" + propertytocompare + "']:checked").val() : $('#' + propertytocompare).val();
           // Allow for unticked checkboxes
           if (params.compareType == "CHECKBOX" && propertytocompareValue == undefined) {
               propertytocompareValue = "false";
           }

           if (propertytocompareValue == null || propertytocompareValue == undefined || propertytocompareValue  == '' )
           {
               return true;
           }

           // If radio button pass in a default value as if it is selected, we have a negative match
           if (params.compareType == "RADIO" || params.compareType == "CHECKBOX")
           {
               if($(element).is(":checked"))
               {
                   if ( $('#' + propertytocompare).is(":checked"))
                   {
                       return false;
                   }
               }
               return true;
           }
           

           for (var i = 0; i < ignorevalues.length; i++) {
               if(ignorevalues[i] == propertytocompareValue)
               {
                   return true;
               }
            }
           return false;
       });

    jQuery.validator.unobtrusive.adapters.add(
        'ifpropertysetnotallowed', ['propertytocompare', 'ignorevalues'], function (options) {
            var params = {
                propertytocompare: options.params.propertytocompare,
                ignorevalues: options.params.ignorevalues,
                compareType: $("input[type='radio'][name='" + options.params.propertytocompare + "']").length > 0 ? "RADIO" : $("input[type='checkbox'][name='" + options.params.propertytocompare + "']").length > 0 ? "CHECKBOX" : "INPUTORSELECT"
            };
            options.rules['ifpropertysetnotallowed'] = params;
            options.messages['ifpropertysetnotallowed'] = options.message;
        });


    // Patch to allow Chrome to work with non us date format.
    jQuery.validator.methods.date = function (value, element) {
        var check = false;
        var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (re.test(value)) {
            var adata = value.split('/');
            var gg = parseInt(adata[0], 10);
            var mm = parseInt(adata[1], 10);
            var aaaa = parseInt(adata[2], 10);
            var xdata = new Date(aaaa, mm - 1, gg);
            if ((xdata.getFullYear() == aaaa)
                    && (xdata.getMonth() == mm - 1)
                    && (xdata.getDate() == gg))
                check = true;
            else
                check = false;
        } else
            check = false;
        return this.optional(element) || check;
    }
}
