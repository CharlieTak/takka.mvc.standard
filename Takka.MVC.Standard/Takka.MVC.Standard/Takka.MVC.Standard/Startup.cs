﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Takka.MVC.Standard.Startup))]
namespace Takka.MVC.Standard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
