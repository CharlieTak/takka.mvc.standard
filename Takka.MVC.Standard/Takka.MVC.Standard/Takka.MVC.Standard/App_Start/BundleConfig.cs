﻿using System.Web;
using System.Web.Optimization;

namespace Takka.MVC.Standard
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var jsBundle = new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Foundation/js/vendor/foundation.js");
            jsBundle.Orderer = new PassThroughBundleOrderer();
            bundles.Add(jsBundle);


            var jsVal = new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/takka/validation.js");
            jsVal.Orderer = new PassThroughBundleOrderer();
            bundles.Add(jsVal);

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            var modernizr = new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*");
            modernizr.Orderer = new PassThroughBundleOrderer();
            bundles.Add(modernizr);

            var bootstrap = new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js");
            bootstrap.Orderer = new PassThroughBundleOrderer();
            bundles.Add(bootstrap);

            var cssBundle = new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Foundation/css/app.css",
                      "~/Foundation/css/foundation.css");
            cssBundle.Orderer = new PassThroughBundleOrderer();
            bundles.Add(cssBundle);
        }
    }
}
