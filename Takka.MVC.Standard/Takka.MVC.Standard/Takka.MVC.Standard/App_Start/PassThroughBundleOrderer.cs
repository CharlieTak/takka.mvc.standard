﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Takka.MVC.Standard
{
    /// <summary>
    /// CLASS USED TO ENSURE BUNDLES RETURNED IN CORRECT ORDER
    /// </summary>
    public class PassThroughBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}