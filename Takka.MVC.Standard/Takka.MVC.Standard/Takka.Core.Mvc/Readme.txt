﻿This dll provides useful validation extensions for data annotations in asp.net mvc.

BOOLEANREQUIRED
By default checkboxes pass the required validator with a value of true OR false. This validator Forces a boolean value (e.g. a checkbox) to be set to true.
Example:-
private bool _ConfirmedTravelExpenses = false;
[Takka.Wave.Framework.Application.Models.Validation.BooleanRequired(ErrorMessage = "Please confirm")]
public bool ConfirmedTravelExpenses
{
    get { return _ConfirmedTravelExpenses; }
    set { _ConfirmedTravelExpenses = value; }
}

UNLIKE
This checks your annotated property value against a list of other properties and if any of them match returns fail.

Example:
private string _Workshop1Id ="";
[Required(ErrorMessage = "Please select a tutorial")]
[Takka.Wave.Framework.Application.Models.Validation.Unlike("Workshop2Id,Workshop3Id,Workshop4Id", "Workshop1", "", ErrorMessage = "Please select 4 different tutorials")]
public string Workshop1Id
{
    get { return _Workshop1Id; }
    set { _Workshop1Id = value; }
}



The javascript you need to add for these 2 validators is below.
if (jQuery.validator) {

	// The boolean required function
    jQuery.validator.unobtrusive.adapters.add("brequired", function (options) {
        //b-required for checkboxes
        if (options.element.tagName.toUpperCase() == "INPUT" && options.element.type.toUpperCase() == "CHECKBOX") {
            //setValidationValues(options, "required", true);
            options.rules["required"] = true;
            if (options.message) {
                options.messages["required"] = options.message;
            }
        }
    });

    // The unlike function
    jQuery.validator.addMethod(
        'unlike',
       function (value, element, params) {
           if (!this.optional(element)) {
               var returnValue = true;
               var props =  params.otherproperty.split(',');
               // Cycle through our unlike properties and see if they match. If any match return error.
               for (var i = 0; i < props.length; i++)
               {
                   var otherProperty = $('#' + props[i]);
                   if (otherProperty.val() == value) {
                       returnValue = false;
                       return returnValue;
                   }
               }
           }
           return true;
       });

    jQuery.validator.unobtrusive.adapters.add(
        'unlike', ['otherproperty', 'otherpropertyname'], function (options) {
            var params = {
                otherproperty: options.params.otherproperty,
                otherpropertyname: options.params.otherpropertyname
            };
            options.rules['unlike'] = params;
            options.messages['unlike'] = options.message;
        });
}