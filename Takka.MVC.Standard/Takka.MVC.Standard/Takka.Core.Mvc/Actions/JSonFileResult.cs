﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Takka.Core.Mvc.Actions
{
    public class JSonFileResult<T> : FileResult
    {
        public JSonFileResult()
            : base("application/json")
        {

        }
        private readonly T _item;
        private readonly string _initialJs;
        private readonly string _closingJs;
        /// <summary>
        /// Send any serializable item to be send back as a js file
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fileDownloadName"></param>
        /// <param name="initalJs">javascript to show before the item (e.g. "var retailers = "</param>
        /// <param name="closingJson">closing json (e.g. ";")</param>
        public JSonFileResult(T item, string fileDownloadName, string initalJs = "", string closingJs = "")
            : base("application/json")
        {
            _item = item;
            FileDownloadName = fileDownloadName;
            _initialJs = initalJs;
            _closingJs = closingJs;
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            var outputStream = response.OutputStream;

            using (var memoryStream = new MemoryStream())
            {
                WriteItem(memoryStream);
                outputStream.Write(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
            }
        }

        private void WriteItem(Stream stream)
        {
            var streamWriter = new StreamWriter(stream, Encoding.UTF8);
            SerializeJson(_item, streamWriter.BaseStream);
            if (!string.IsNullOrEmpty(_closingJs)) streamWriter.Write(_closingJs);
            streamWriter.Flush();
        }

        public void SerializeJson(T value, Stream s)
        {
            StreamWriter writer = new StreamWriter(s);
            if (!string.IsNullOrEmpty(_initialJs)) writer.Write(_initialJs);
            JsonTextWriter jsonWriter = new JsonTextWriter(writer);
            JsonSerializer ser = new JsonSerializer();
            ser.Serialize(jsonWriter, value);
            jsonWriter.Flush();
        }
    }
}
