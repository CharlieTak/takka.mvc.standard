﻿using System;
using System.Web;

namespace Takka.Core.Mvc.Http
{
    public static class EncryptedCookie
    {
        public static void Encrypt(this HttpCookie cookie)
        {
            try
            {
                if (cookie.Values.Count > 0)
                {
                    foreach (string key in cookie.Values.AllKeys)
                    {
                        byte[] data = System.Text.Encoding.UTF8.GetBytes(cookie.Values[key]);
                        byte[] encData = System.Web.Security.MachineKey.Protect(data, "cookie");
                        cookie.Values[key] = Convert.ToBase64String(encData);
                    }
                }
                else
                {
                    byte[] data = System.Text.Encoding.UTF8.GetBytes(cookie.Value);
                    byte[] encData = System.Web.Security.MachineKey.Protect(data, "cookie");
                    cookie.Value = Convert.ToBase64String(encData);
                }
            }
            catch (Exception ex)
            {
                throw new HttpException("Unable to encrypt the cookie.", ex);
            }

        }

        public static void Decrypt(this HttpCookie cookie)
        {
            try
            {
                if (cookie.Values.Count > 0)
                {
                    foreach (string key in cookie.Values.AllKeys)
                    {
                        if (cookie.Values[key].Length != 0)
                        {
                            string val = cookie.Values[key];
                            byte[] decrypted = System.Web.Security.MachineKey.Unprotect(Convert.FromBase64String(val), "cookie");
                            cookie.Values[key] = System.Text.Encoding.UTF8.GetString(decrypted);
                        }
                    }
                }
                else
                {
                    //nothing to do!
                    if (cookie.Value.Length == 0) return;
                    byte[] decrypted = System.Web.Security.MachineKey.Unprotect(Convert.FromBase64String(cookie.Value), "cookie");
                    cookie.Value = System.Text.Encoding.UTF8.GetString(decrypted);
                }
            }
            catch (Exception ex)
            {
                throw new HttpException("Unable to Decrypt the cookie.", ex);
            }
        }
    }
}