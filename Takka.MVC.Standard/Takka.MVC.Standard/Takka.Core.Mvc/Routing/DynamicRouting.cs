﻿using System;
using System.Web.Routing;
using System.Web.Mvc;

namespace Takka.Core.Mvc.Routing
{
    /// <summary>
    /// *************************************************************************
    /// Class to allow routes to be added and removed programmatically - for example when adding or removing pages in a CMS. This saves restarting the application.
    /// *************************************************************************
    /// This class helps us with our route mapping. Becuase the route name is not exposed in the routetable, we need something to do it for us
    /// otherwise we have to restart the website whenever a url changes on a managed page. This class uses "Data tokens" which you can attach to routes handily
    /// Fortunately this microsoft bod was here to help us add the route datato save us writing it iourselves...
    /// http://haacked.com/archive/2010/11/28/getting-the-route-name-for-a-route.aspx/
    /// </summary>
    /// <example>
    ///  public static void RebuildRoute(HttpContextBase context, RouteCollection routes, PageModel page)
        //{
        //    var existingRoutes = RouteTable.Routes.GetRouteData(context);
        //    if (RouteTable.Routes.GetRouteByName(page.id.ToString()) != null)
        //    {
        //       RouteTable.Routes.Remove(RouteTable.Routes[page.id.ToString()]);
        //    }
        //    if (page.Enabled)
        //    {
        //        routes.Map(name: page.id.ToString(),
        //                    url: page.Url,
        //                    defaults: new { controller = "Site", action = "SitePage" });
        //        // once we have registered the route, move it to the top of the table otherwise
        //        // the default routing will kick in and we lose it.
        //        Route route = RouteTable.Routes.GetRouteByName(page.id.ToString());
        //        RouteTable.Routes.Remove(route);
        //        RouteTable.Routes.Insert(0, route);
        //    }
        //}
    /// </example>
    public static class DynamicRouting
    {
        /// <summary>
        /// Now this is our method to get the route back
        /// </summary>
        /// <param name="routes"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Route GetRouteByName(this RouteCollection routes, string name)
        {
            foreach (var r in routes)
            {
                if (r is Route)
                {
                    Route route = r as Route;
                    if (route.DataTokens != null && route.GetRouteName() == name)
                    {
                        return route;
                    }
                }
            }

            return null;
        }

        #region Route name helpers
        public static Route Map(this RouteCollection routes, string name,
    string url)
        {
            return routes.Map(name, url, null, null, null);
        }

        public static Route Map(this RouteCollection routes, string name,
            string url, object defaults)
        {
            return routes.Map(name, url, defaults, null, null);
        }

        public static Route Map(this RouteCollection routes, string name,
            string url, object defaults, object constraints)
        {
            return routes.Map(name, url, defaults, constraints, null);
        }

        public static Route Map(this RouteCollection routes, string name,
            string url, object defaults, object constraints, string[] namespaces)
        {
            return routes.MapRoute(name, url, defaults, constraints, namespaces)
              .SetRouteName(name);
        }

        public static string GetRouteName(this Route route)
        {
            if (route == null)
            {
                return null;
            }
            return route.DataTokens.GetRouteName();
        }

        public static string GetRouteName(this RouteData routeData)
        {
            if (routeData == null)
            {
                return null;
            }
            return routeData.DataTokens.GetRouteName();
        }

        public static string GetRouteName(this RouteValueDictionary routeValues)
        {
            if (routeValues == null)
            {
                return null;
            }
            object routeName = null;
            routeValues.TryGetValue("__RouteName", out routeName);
            return routeName as string;
        }

        public static Route SetRouteName(this Route route, string routeName)
        {
            if (route == null)
            {
                throw new ArgumentNullException("route");
            }
            if (route.DataTokens == null)
            {
                route.DataTokens = new RouteValueDictionary();
            }
            route.DataTokens["__RouteName"] = routeName;
            return route;
        }

        #endregion

    }
}