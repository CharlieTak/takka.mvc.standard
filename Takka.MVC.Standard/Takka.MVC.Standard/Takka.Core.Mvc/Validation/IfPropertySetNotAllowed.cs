﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Takka.Core.Mvc.Validation
{
    /// <summary>
    /// Validation attribute to indicate that a property field or parameter is not allowed, but ONLY if
    /// the supplied other property is sey.
    /// The javascript needed to support this is
    /// The if property set not allowed function
    //jQuery.validator.addMethod(
    //    'ifpropertysetnotallowed',
    //   function(value, element, params)
    //{
    //    var returnValue = true;
    //    var propertytocompare = params.propertytocompare;
    //    var ignorevalues = params.ignorevalues;
    //    var propertytocompareValue = params.compareType == "RADIO" ? $("input[type='radio'][name='" + propertytocompare + "']:checked").val() : params.compareType == "CHECKBOX" ? $("input[type='checkbox'][name='" + propertytocompare + "']:checked").val() : $('#' + propertytocompare).val();
    //    Allow for unticked checkboxes
    //    if (params.compareType == "CHECKBOX" && propertytocompareValue == undefined) {
    //        propertytocompareValue = "false";
    //    }
    //    if (propertytocompareValue == null || propertytocompareValue == undefined || propertytocompareValue == '')
    //    {
    //        return true;
    //    }

    //    for (var i = 0; i < ignorevalues.length; i++)
    //    {
    //        if (ignorevalues[i] == propertytocompareValue)
    //        {
    //            return true;
    //        }
    //    }
    //    return false;
    //});

    //jQuery.validator.unobtrusive.adapters.add(
    //    'ifpropertysetnotallowed', ['propertytocompare', 'ignorevalues'], function(options)
    //{
    //    var params = {
    //        propertytocompare: options.params.propertytocompare,
    //            ignorevalues: options.params.ignorevalues,
    //            compareType: $("input[type='radio'][name='" + options.params.propertytocompare + "']").length > 0 ? "RADIO" : $("input[type='checkbox'][name='" + options.params.propertytocompare + "']").length > 0 ? "CHECKBOX" : "INPUTORSELECT"
    //        };
    //    options.rules['ifpropertysetnotallowed'] = params;
    //    options.messages['ifpropertysetnotallowed'] = options.message;
    //});

    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class IfPropertySetNotAllowed : ValidationAttribute, IClientValidatable
    {
        public string PropertyToCompare { get; private set; }
        public string[] IgnoreValues { get; private set; }
        private const string DefaultErrorMessage = "The field {0} is not allowed";

        /// <summary>
        /// Allows an attribute to be required ONLY if the property to compare has a given value
        /// </summary>
        /// <param name="propertyToCompare">The property to be checked</param>
        /// <param name="ignoreValue">List of values that can be ignored on the other property</param>
        public IfPropertySetNotAllowed(string propertyToCompare, string[] ignoreValues = null) : base(DefaultErrorMessage)
        {
            PropertyToCompare = propertyToCompare;
            IgnoreValues = ignoreValues;
        }


        /// <summary>
        /// Override of <see cref="ValidationAttribute.IsValid(object)"/>
        /// </summary>
        /// <param name="value">The value to test</param>
        /// <returns><c>false</c> if the <paramref name="value"/> is null or an empty string. If <see cref="RequiredAttribute.AllowEmptyStrings"/>
        /// then <c>false</c> is returned only if <paramref name="value"/> is null.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // only check string length if empty strings are not allowed
            var stringValue = value as string;

            var otherProperty = validationContext.ObjectInstance.GetType().GetProperty(PropertyToCompare);
            var otherPropertyValue = otherProperty.GetValue(validationContext.ObjectInstance, null);

            if (otherPropertyValue == null || otherPropertyValue.ToString() == string.Empty || (IgnoreValues == null || IgnoreValues.Contains(otherPropertyValue.ToString())))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }
        }


        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule()
            {
                ValidationType = "ifpropertysetnotallowed",
                ErrorMessage = !string.IsNullOrEmpty(this.ErrorMessage) ? this.ErrorMessage : FormatErrorMessage(metadata.DisplayName)
            };

            rule.ValidationParameters.Add("ignorevalues", IgnoreValues);

            rule.ValidationParameters.Add("propertytocompare", PropertyToCompare);

            yield return rule;
        }
    }
}