﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Takka.Core.Mvc.Validation
{
    /// <summary>
    /// Accepts a comma separated list of properties to check against. If any return a match, valid is false
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class Unlike : ValidationAttribute, IClientValidatable
    {
        private const string DefaultErrorMessage = "The value of {0} cannot be the same as the value of the {1}.";

        public string OtherProperty { get; private set; }
        public string OtherPropertyName { get; private set; }
        public string IgnoreValue { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="otherProperty">Properties to compare (comma delimited)</param>
        /// <param name="otherPropertyName">Name of the property for error message</param>
        /// <param name="ignoreValue">Ignore the validation if the string value matches this</param>
        public Unlike(
            string otherProperty,
            string otherPropertyName,
            string ignoreValue = "")
            : base(DefaultErrorMessage)
        {
            OtherProperty = otherProperty;
            OtherPropertyName = otherPropertyName;
            IgnoreValue = ignoreValue;
        }
        
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null && (string.IsNullOrEmpty(IgnoreValue) || value.ToString() != IgnoreValue))
            {
                // Cycle through the properties and return failure if any of them match
                foreach (var op in OtherProperty.Split(','))
                {
                    var otherProperty = validationContext.ObjectInstance.GetType().GetProperty(op);

                    var otherPropertyValue = otherProperty.GetValue(validationContext.ObjectInstance, null);

                    if (value.Equals(otherPropertyValue))
                    {
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                    }
                }
            }

            return ValidationResult.Success;

        }
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule()
            {
                ValidationType = "unlike",
                ErrorMessage = this.ErrorMessage
            };

            rule.ValidationParameters.Add("otherproperty", OtherProperty);
            rule.ValidationParameters.Add("otherpropertyname", OtherPropertyName);
            rule.ValidationParameters.Add("ignorevalue", IgnoreValue);
            rule.ValidationParameters.Add("propertyname", metadata.PropertyName);

            yield return rule;
        }

        public override string FormatErrorMessage(string name)
        {
            try
            {
                return string.Format(ErrorMessageString, name, OtherPropertyName);
            }
            catch (FormatException)
            {
                return ErrorMessageString;
            }
        }
    }
}
