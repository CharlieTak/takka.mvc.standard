﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Takka.Core.Mvc.Validation
{
    /// <summary>
    /// Set this in the annotation for a property to false a boolean field to be required
    /// </summary>
    public class BooleanRequired : RequiredAttribute, IClientValidatable
    {

        public BooleanRequired()
        {

        }

        public override bool IsValid(object value)
        {
            return value != null && (bool)value == true;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new ModelClientValidationRule[] { new ModelClientValidationRule() { ValidationType = "brequired", ErrorMessage = this.ErrorMessage } };
        }
    }
}
