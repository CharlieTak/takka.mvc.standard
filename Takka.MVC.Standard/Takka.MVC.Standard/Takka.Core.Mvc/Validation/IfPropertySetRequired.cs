﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Takka.Core.Mvc.Validation
{
    /// <summary>
    /// Validation attribute to indicate that a property field or parameter is required, but ONLY if
    /// the supplied other property has a given value.
    /// The javascript needed to support this is
    /// The if property set required function
    ///jQuery.validator.addMethod(
    ///    'ifpropertysetrequired',
    ///   function(value, element, params)
    ///{
    ///    var returnValue = true;
    ///    var propertytocompare = params.propertytocompare;
    ///    var propertyvalueforrequired = params.propertyvalueforrequired;
    ///    var propertytocompareValue = params.compareType == "RADIO" ? $("input[type='radio'][name='" + propertytocompare + "']:checked").val() : $('#' + propertytocompare).val();
    ///    if (propertytocompareValue == propertyvalueforrequired)
    ///    {
    ///        if (value == null || value == undefined || value.length == 0)
    ///        {
    ///            return false;
    ///        }
    ///    }
    ///    return true;
    ///});

    ///jQuery.validator.unobtrusive.adapters.add(
    ///    'ifpropertysetrequired', ['propertytocompare', 'propertyvalueforrequired'], function(options)
    ///{
    ///    var params = {
    ///        propertytocompare: options.params.propertytocompare,
    ///            propertyvalueforrequired: options.params.propertyvalueforrequired,
    ///            compareType: $("input[type='radio'][name='" + options.params.propertytocompare + "']").length > 0 ? "RADIO" : "INPUTORSELECT"
    ///        };
    ///    options.rules['ifpropertysetrequired'] = params;
    ///    options.messages['ifpropertysetrequired'] = options.message;
    ///});
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class IfPropertySetRequired : ValidationAttribute, IClientValidatable
    {
        public string PropertyToCompare { get; private set; }
        public object PropertyValueForRequired { get; private set; }
        private const string DefaultErrorMessage = "The field {0} is required";

        /// <summary>
        /// Allows an attribute to be required ONLY if the property to compare has a given value
        /// </summary>
        /// <param name="propertyToCompare">The property to be checked</param>
        /// <param name="propertyValueForRequired">The vaslue of the property to be checked in order for the required validation to be added in</param>
        public IfPropertySetRequired(string propertyToCompare, object propertyValueForRequired) : base(DefaultErrorMessage)
        {
            PropertyToCompare = propertyToCompare;
            PropertyValueForRequired = propertyValueForRequired;
        }


        /// <summary>
        /// Override of <see cref="ValidationAttribute.IsValid(object)"/>
        /// </summary>
        /// <param name="value">The value to test</param>
        /// <returns><c>false</c> if the <paramref name="value"/> is null or an empty string. If <see cref="RequiredAttribute.AllowEmptyStrings"/>
        /// then <c>false</c> is returned only if <paramref name="vale"/> is null.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // only check string length if empty strings are not allowed
            var stringValue = value as string;

            var otherProperty = validationContext.ObjectInstance.GetType().GetProperty(PropertyToCompare);
            var otherPropertyValue = otherProperty.GetValue(validationContext.ObjectInstance, null);
            
            if(otherPropertyValue == null && PropertyValueForRequired != null)
            {
                return ValidationResult.Success;
            }

            if (otherPropertyValue == null && PropertyValueForRequired == null || otherPropertyValue.Equals(PropertyValueForRequired))
            {
                if (stringValue == null || stringValue.Trim().Length == 0)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule()
            {
                ValidationType = "ifpropertysetrequired",
                ErrorMessage = !string.IsNullOrEmpty(this.ErrorMessage) ? this.ErrorMessage : FormatErrorMessage(metadata.DisplayName)
            };


            if (PropertyValueForRequired is bool)
            {
                rule.ValidationParameters.Add("propertyvalueforrequired", PropertyValueForRequired.ToString().ToLower());
            }
            else
            {
                rule.ValidationParameters.Add("propertyvalueforrequired", PropertyValueForRequired);
            }
            rule.ValidationParameters.Add("propertytocompare", PropertyToCompare);

            yield return rule;
        }
    }
}